News
====

2011-11-15
----------
  * Completed packaging for pypi server (readme, news, todo, setup.py, tests moved)
  * buildout support added
  * uploaded to pypi server.

2011-05-09
----------
  * installation script and readme created 
  * Test code moved to seperate directory.
  * zip compression is now not storing, but really compressing the files.

2010-11-21
----------
  Initial version
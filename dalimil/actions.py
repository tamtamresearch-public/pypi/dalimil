from pprint import pprint
import zipfile
import shutil

import os


def do_list(archmanager):
    """Check for file name duplicates in archives.
    """
    # TODO: following call is apparently broken
    list(archmanager, check_duplicates=True)
    return


def list_notimefiles(archmanager):
    print("==Files without detected time==")
    if len(archmanager.notimefiles) == 0:
        print("None found")
    else:
        pprint(archmanager.notimefiles)
    return


def list_duplicates(archmanager):
    print("==Files with duplicated names per archive")
    if len(archmanager.archs_with_duplicates) == 0:
        print("None found")
    else:
        pprint(archmanager.archs_with_duplicates)
    return


def list_skippedfiles(archmanager):
    """Print files skipped due to skipping current and future archives.
    """
    print("==Files skipped as they belong to current and future archives==")
    if len(archmanager.skippedfiles) == 0:
        print("None found")
    else:
        pprint(archmanager.skippedfiles)
    return


# TODO: fix: redefinition of stdlib list function
def list(archmanager, check_duplicates=True):
    """Lists files, gives option if check_duplicates is called on archmanager.
    """
    if check_duplicates:
        # Check for file name duplicates in archives
        archmanager.check_duplicates()

    # list files, which had no time detected
    list_notimefiles(archmanager)
    # print duplicates, if we care about it
    if check_duplicates:
        list_duplicates(archmanager)

    # print files skipped due to skipping current and future archives
    list_skippedfiles(archmanager)
    # print files per archive
    print("==Files to be archived per archive")
    if len(archmanager.archives) == 0:
        print("None found")
    else:
        pprint(archmanager.archives)
    return


def do_copy2dir(archmanager, delete=False):
    mode = "move" if delete else "copy"
    # Check for file name duplicates in archives
    archmanager.check_duplicates()
    # TODO: list files which are notime or duplicates
    print("Files duplicated in container and files "
          "without detected time are always skipped:")
    list_duplicates(archmanager)
    list_notimefiles(archmanager)
    if len(archmanager.skippedfiles) > 0:
        list_skippedfiles(archmanager)
    print("==Moving/copying files==")
    for archname, files in archmanager.archives.items():
        try:
            if os.path.exists(archname):
                orgpath = archname
                archname = unique_filename(archname)
                msg = ("Directory {orgpath} already exists, "
                       "using modified name {newpath}")
                print(msg.format(orgpath=orgpath, newpath=archname))
            os.makedirs(archname)
        except:
            print("Unable to create directory {0}".format(archname))
            continue
        for file in files:
            try:
                destfile = archname + "/" + os.path.split(file)[1]
                shutil.copy2(file, destfile)
                if delete:
                    os.remove(file)
                msg = "{stat}: {mode} {file} => {destfile}"
                print(msg.format(stat="OK",
                                 file=file,
                                 destfile=destfile,
                                 mode=mode))
            except:
                msg = "{stat}: {mode} {file} => {destfile}"
                print(msg.format(stat="ERR",
                                 file=file,
                                 destfile=destfile, mode=mode))
                continue
    return


def do_move2dir(archmanager):
    return do_copy2dir(archmanager, delete=True)


def do_move2zip(archmanager):
    return do_copy2zip(archmanager, delete=True)


def do_copy2zip(archmanager, delete=False):
    # Check for file name duplicates in archives
    archmanager.check_duplicates()
    # TODO: list files which are notime or duplicates
    print("Files duplicated in container and files "
          "without detected time are always skipped:")
    list_duplicates(archmanager)
    list_notimefiles(archmanager)
    if len(archmanager.skippedfiles) > 0:
        list_skippedfiles(archmanager)
    print("==Copying files==")
    for archname, files in archmanager.archives.items():
        success = False
        if os.path.exists(archname):
            orgpath = archname
            archname = unique_filename(archname)
            msg = ("Archive {orgpath} already exists, "
                   "using modified name {newpath}")
            print(msg.format(orgpath=orgpath, newpath=archname))
        try:
            archdir = os.path.split(archname)[0]
            if os.path.exists(archdir):
                pass
            else:
                os.makedirs(archdir)
        except:
            print("Unable to create directory {0}".format(archdir))
            continue
        try:
            with zipfile.ZipFile(archname, 'w') as zip:
                for file in files:
                    zip.write(file, arcname=os.path.split(file)[1],
                              compress_type=zipfile.ZIP_DEFLATED)
                msg = "OK: Archive created: {archive}"
                print(msg.format(archive=archname))
                print("Following files were archived:")
                pprint(files)
                success = True
        except:
            msg = "ERROR: Unable to create archive {archive}"
            print(msg.format(archive=archname))
            print("Hint: zlib module is told to be required for "
                  "effective compression, if missing, we can fail.")
            print("Following files were skipped:")
            pprint(files)
        if delete and success:
            print("Deleting source files:")
            success = False
            for file in files:
                try:
                    os.remove(file)
                    # Checking, if at least one file was succesfully deleted.
                    success = True
                except:
                    msg = "{stat}: Unable to delete file {file}"
                    print(msg.format(stat="ERR", file=file))
                    continue
            if success:
                print("..deleted.")
    return


def unique_filename(file_name):
    counter = 1
    file_name_parts = os.path.splitext(file_name)
    pattern = "{file_name_parts[0]}_{counter}{file_name_parts[1]}"
    while os.path.exists(file_name):
        # file_name = (file_name_parts[0] + '_' +
        #              str(counter) + file_name_parts[1])
        file_name = pattern.format(file_name_parts=file_name_parts,
                                   counter=counter)
        counter += 1
    return file_name


def not_implemented_yet(archmanager):
    print("Action not implemented yet.")
    return


do_move2targz = not_implemented_yet
do_copy2targz = not_implemented_yet

# dictionary of actions
actions = {"list": do_list,
           "move2dir": do_move2dir,
           "move2zip": do_move2zip,
           "move2targz": do_move2targz,
           "copy2dir": do_copy2dir,
           "copy2zip": do_copy2zip,
           "copy2targz": do_copy2targz}
